<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>selectHotelCenturyPark</name>
   <tag></tag>
   <elementGuidId>8ec5406d-1e8f-4e3b-ba70-c08db2ca6cc3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'Century Park Hotel']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[3]/main/div/div[2]/div[2]/div[2]/div[2]/div/a/div/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Century Park Hotel</value>
   </webElementProperties>
</WebElementEntity>
