$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/arief.hakim/git/tiket.com/katalon-web-automation-tiket/./Include/features/tiket/bookHotel.feature");
formatter.feature({
  "name": "bookHotel",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "bookHotel",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user klik hotel menu",
  "keyword": "Given "
});
formatter.match({
  "location": "bookHotel.user_klik_hotel_menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik destination",
  "keyword": "When "
});
formatter.match({
  "location": "bookHotel.user_klik_destination()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select Jakarta",
  "keyword": "Then "
});
formatter.match({
  "location": "bookHotel.user_select_Jakarta()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik check in date",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_klik_check_in_date()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select check in date",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_select_check_in_date()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik check out date",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_klik_check_out_date()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select check out date",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_select_check_out_date()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik guest and room",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_klik_guest_and_room()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik selesai",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_klik_selesai()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik search hotel",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.user_klik_search_hotel()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify landing page search hotel",
  "keyword": "And "
});
formatter.match({
  "location": "bookHotel.verify_landing_page_search_hotel()"
});
formatter.result({
  "status": "passed"
});
});