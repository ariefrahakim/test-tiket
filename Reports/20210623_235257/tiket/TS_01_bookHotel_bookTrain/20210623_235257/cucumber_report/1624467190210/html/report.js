$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/arief.hakim/git/tiket.com/katalon-web-automation-tiket/./Include/features/tiket/loginTiketWithFB.feature");
formatter.feature({
  "name": "bookHotel",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "bookHotel",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "go to url login tiket",
  "keyword": "Given "
});
formatter.match({
  "location": "loginTiket.go_to_url_login_tiket()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik login with facebook",
  "keyword": "When "
});
formatter.match({
  "location": "loginTiket.user_klik_login_with_facebook()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input email",
  "keyword": "Then "
});
formatter.match({
  "location": "loginTiket.user_input_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input password",
  "keyword": "And "
});
formatter.match({
  "location": "loginTiket.user_input_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik login",
  "keyword": "And "
});
formatter.match({
  "location": "loginTiket.user_klik_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify landing page login",
  "keyword": "And "
});
formatter.match({
  "location": "loginTiket.verify_landing_page_login()"
});
formatter.result({
  "status": "passed"
});
});