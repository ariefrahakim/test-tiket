$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/arief.hakim/git/tiket.com/katalon-web-automation-tiket/./Include/features/tiket/Train/searchTiketKAI.feature");
formatter.feature({
  "name": "searchTiketKAI",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "searchTiketKAI",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user klik menu kai",
  "keyword": "Given "
});
formatter.match({
  "location": "searchTiketKAI.user_klik_menu_kai()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input stasiun keberangkatan",
  "keyword": "When "
});
formatter.match({
  "location": "searchTiketKAI.user_input_stasiun_keberangkatan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select stasiun keberangkatan",
  "keyword": "Then "
});
formatter.match({
  "location": "searchTiketKAI.user_select_stasiun_keberangkatan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input stasiun tujuan",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.user_input_stasiun_tujuan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select stasiun tujuan",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.user_select_stasiun_tujuan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select tanggal pergi",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.user_select_tanggal_pergi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user add penumpang",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.user_add_penumpang()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik selesai kai",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.user_klik_selesai_kai()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user cari kai",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.user_cari_kai()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify list kai",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.verify_list_kai()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "klik ekonomi filter",
  "keyword": "And "
});
formatter.match({
  "location": "searchTiketKAI.klik_ekonomi_filter()"
});
formatter.result({
  "status": "passed"
});
});