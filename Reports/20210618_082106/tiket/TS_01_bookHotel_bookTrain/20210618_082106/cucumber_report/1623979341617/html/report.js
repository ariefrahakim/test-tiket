$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/arief.hakim/git/tiket.com/katalon-web-automation-tiket/./Include/features/tiket/filterHotel.feature");
formatter.feature({
  "name": "filterHotel",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "filterHotel",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user klik select bintang hotel",
  "keyword": "Given "
});
formatter.match({
  "location": "filterHotel.user_klik_select_bintang_hotel()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik select bintang",
  "keyword": "When "
});
formatter.match({
  "location": "filterHotel.user_klik_select_bintang()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik span filter",
  "keyword": "Then "
});
formatter.match({
  "location": "filterHotel.user_klik_span_filter()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify after select bintang",
  "keyword": "And "
});
formatter.match({
  "location": "filterHotel.verify_after_select_bintang()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik sample hotel",
  "keyword": "And "
});
formatter.match({
  "location": "filterHotel.user_klik_sample_hotel()"
});
formatter.result({
  "status": "passed"
});
});