$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/arief.hakim/git/tiket.com/katalon-web-automation-tiket/./Include/features/tiket/payHotel.feature");
formatter.feature({
  "name": "payHotel",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "payHotel",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "verify selesaikan pembayaran",
  "keyword": "Given "
});
formatter.match({
  "location": "payHotel.verify_selesaikan_pembayaran()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik atm bersama",
  "keyword": "When "
});
formatter.match({
  "location": "payHotel.user_klik_atm_bersama()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify atm bersama present",
  "keyword": "Then "
});
formatter.match({
  "location": "payHotel.verify_atm_bersama_present()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik bayar",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_bayar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik lihat daftar pesanan",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_lihat_daftar_pesanan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik ke my order",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_ke_my_order()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik ok riwayat pesanan",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_ok_riwayat_pesanan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik page profile",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_page_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik keluar",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_keluar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik ya keluar",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.user_klik_ya_keluar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify landing page logout",
  "keyword": "And "
});
formatter.match({
  "location": "payHotel.verify_landing_page_logout()"
});
formatter.result({
  "status": "passed"
});
});