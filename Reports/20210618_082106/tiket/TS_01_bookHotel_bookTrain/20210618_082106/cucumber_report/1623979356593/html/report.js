$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/arief.hakim/git/tiket.com/katalon-web-automation-tiket/./Include/features/tiket/selectRoom.feature");
formatter.feature({
  "name": "selectRoom",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "selectRoom",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "verify landing page select hotel",
  "keyword": "Given "
});
formatter.match({
  "location": "selectRoom.verify_landing_page_select_hotel()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik lihat kamar",
  "keyword": "When "
});
formatter.match({
  "location": "selectRoom.user_klik_lihat_kamar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik pilih kamar",
  "keyword": "Then "
});
formatter.match({
  "location": "selectRoom.user_klik_pilih_kamar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik pesan kamar",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_klik_pesan_kamar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik title detail pemesanan",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_klik_title_detail_pemesanan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select title detail pemesanan",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_select_title_detail_pemesanan()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik title detail tamu",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_klik_title_detail_tamu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select title detail tamu",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_select_title_detail_tamu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik bebas asap rokok",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_klik_bebas_asap_rokok()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user klik lanjut ke pembayaran",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_klik_lanjut_ke_pembayaran()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user verify landing page after pembayaran",
  "keyword": "And "
});
formatter.match({
  "location": "selectRoom.user_verify_landing_page_after_pembayaran()"
});
formatter.result({
  "status": "passed"
});
});