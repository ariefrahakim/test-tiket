package tiket

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class detailPemesananKAI {

	@Given("user klik pilih kereta")
	def user_klik_pilih_kereta() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/buttonPilihKAI'))
		WebUI.delay(1)
	}

	@When("verify landing page pilih kereta")
	def verify_landing_page_pilih_kereta() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/landingPagePilihKAI'), 0)
		WebUI.delay(1)
	}

	@Then("user klik sama dengan pemesan")
	def user_klik_sama_dengan_pemesan() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/samaDenganPemesan'))
		WebUI.delay(1)
	}

	@And("user klik nama penumpang2")
	def user_klik_nama_penumpang2() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/inputPenumpangBayi'))
		WebUI.delay(1)
	}

	@And("user input nama bayi")
	def user_input_nama_bayi() {
		WebUI.setText(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/inputPenumpangBayi'), 'adelard')
		WebUI.delay(1)
	}

	@And("user klik dateofbirth")
	def user_klik_dateofbirth() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/inputTitleBirthDate'))
		WebUI.delay(1)
	}

	@And("user klik tanggal bayi")
	def user_klik_tanggal_bayi() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/tanggal1Lahir'))
		WebUI.delay(1)
	}

	@And("user klik bulan bayi")
	def user_klik_bulan_bayi() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/divAgt'))
		WebUI.delay(1)
	}

	@And("user klik tahun bayi")
	def user_klik_tahun_bayi() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/div2018'))
		WebUI.delay(1)
	}

	@And("user klik pilih kursi")
	def user_klik_pilih_kursi() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/buttonPilihKursi'))
		WebUI.delay(1)
	}

	@And("user klik seat")
	def user_klik_seat() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/pilihSeatPenumpang'))
		WebUI.delay(1)
	}

	@And("user klik lanjut pembayaran")
	def user_klik_lanjut_pembayaran() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/buttonLanjutkanPembayaranKAI'))
		WebUI.delay(1)
	}

	@And("lanjutkan ke pembayaran")
	def lanjutkan_ke_pembayaran() {
		WebUI.click(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/buttonYaLanjutkanKAI'))
		WebUI.delay(1)
	}

	@And("verify pembayaran kai")
	def verify_pembayaran_kai() {
		WebUI.verifyElementPresent(findTestObject('Object Repository/BookKeretaApi/detailPemesananKAI/verifyMetodePembayaranKAI'), 0)
		WebUI.delay(1)
	}
}