package tiket

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class payHotel {

	@Given("verify selesaikan pembayaran")
	def verify_selesaikan_pembayaran() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Hotel/Bayar/verifySelesaikanPembayaran'))
		WebUI.delay(2)
	}

	@When("user klik atm bersama")
	def user_klik_atm_bersama() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/spanATMBersama'))
		WebUI.delay(1)
	}

	@Then("verify atm bersama present")
	def verify_atm_bersama_present() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Hotel/Bayar/ATMBersamaVerify'))
		WebUI.delay(2)
	}

	@And("user klik bayar")
	def user_klik_bayar() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/buttonBayar'))
		WebUI.delay(2)
	}

	@And("user klik lihat daftar pesanan")
	def user_klik_lihat_daftar_pesanan() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/buttonLihatDaftarPesanan'))
		WebUI.delay(2)
	}

	@And("user klik ke my order")
	def user_klik_ke_my_order() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/buttonKeMyOrder'))
		WebUI.delay(2)
	}

	@And("user klik ok riwayat pesanan")
	def user_klik_ok_riwayat_pesanan() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/buttonOKRiwayatPemesanan'))
		WebUI.delay(2)
	}

	@And("user klik page profile")
	def user_klik_page_profile() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/spanPageProfile'))
		WebUI.delay(2)
	}

	@And("user klik keluar")
	def user_klik_keluar() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/spanKeluar'))
		WebUI.delay(2)
	}

	@And("user klik ya keluar")
	def user_klik_ya_keluar() {
		WebUI.click(findTestObject('Object Repository/Hotel/Bayar/yaKeluar'))
		WebUI.delay(2)
	}

	@And("verify landing page logout")
	def verify_landing_page_logout() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Hotel/Bayar/landingPageLogout'))
		WebUI.delay(1)
		WebUI.closeBrowser()
	}
}